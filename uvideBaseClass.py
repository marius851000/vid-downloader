from getClass import part

class everyThing(part):
    def __init__(self):
        part.__init__(self,None,"")

    def getChilds(self):
        return []

    def get(self,sf):
        # TODO: automatise this
        if sf == "tf1":
            import getter.tf1 as tf1
            return tf1.mainclass(self,sf)
        elif sf == "blendercloud":
            import getter.blendercloud as blendercloud
            return blendercloud.mainclass(self,sf)
        elif sf == "arte":
            import getter.arte as arte
            return arte.mainclass(self,sf)
        elif sf == "futurama-streaming":
            import getter.futuramastreaming as futuramastreaming
            return futuramastreaming.mainclass(self,sf)

        else:
            raise BaseException("unexistant module : "+sf)

    def getAll(self):
        return ["tf1","blendercloud","futurama-streaming"]
