from tool import parseHtml

def get_mystream_download(url):
    page = parseHtml(url, dump = True)
    url = page.xpath("//source")[0].attrib["src"]
    return {"type": "url",
        "url": url,
        "extension": url.split(".")[-1]}

#print(get_mystream_download("https://mstream.cloud/zz2geugwbv01"))
