from getClass import part, video
from tool import parseHtml
from extracttool import get_mystream_download
BASE_URL = "http://www.futurama-streaming.com"

def get_seasons():
    season_url = "http://www.futurama-streaming.com/saisons"

    page = parseHtml(season_url)
    content = page.xpath("//section[@id='conteneur_gauche']/div[@class='panel panel-default']/div[@class='panel-body']/div[@class='list-group']/a")
    retour = {}
    for loop in content:
        text = loop.xpath("./text()")[0]
        season = int(text.split("Saisons : ")[1].split(" (")[0])
        url = BASE_URL + loop.attrib["href"]
        retour[season] = {
                "url" : url
            }
    return retour

def get_episodes(base_url, season_id):
    url = base_url + "saison-"+str(season_id)
    page = parseHtml(url)
    episodes_section = page.xpath("//section[@id='conteneur_gauche']/div[@class='panel panel-default']")
    data = {}
    for loop in episodes_section:
        name = loop.xpath("./div[@class='panel-heading']/div/div/a[@class='a-panel-head-gauche']/text()")[1].split(" : ")[1]
        url = base_url[0:-1] + loop.xpath("./div[@class='panel-heading']/div/div/a[@class='a-panel-head-gauche']")[1].attrib["href"]
        adata = {
            "name":name,
            "url":url
        }
        data[name] = adata
    return data

def get_episode_data(url):
    page = parseHtml(url)
    data = {}

    data["name"] = page.xpath("//title/text()")[0].split(" : ")[1].split(" - Futurama Streaming")[0]
    data["season"] = page.xpath("//title/text()")[0].split("Saison ")[1].split(" ")[0]
    data["episode"] = page.xpath("//title/text()")[0].split("Episode ")[1].split(" ")[0]
    data["description"] = page.xpath("//div[@class='panel-body']/p/text()")[0]
    data["id"] = page.xpath("//script/text()")[2].split("/lecteur/")[1].split("/?id")[0]
    return data

class episodeclass(video):
    base_url="http://www.futurama-streaming.com/"
    # TODO: add the possibility to have multiple stream
    # TODO: other than mystream

    def parse(self):
        data = get_episode_data(self.episode_url)
        mystream_page = parseHtml(self.episode_url, dump = True )
        mystream_url = mystream_page.xpath("//iframe[@webkitallowfullscreen='true']")[0].attrib.get("src")
        mystream_download = get_mystream_download(mystream_url)

        self.videoGetData = mystream_download
        self.name = data["name"]
        self.episode = data["episode"]
        self.saison = data["season"]
        self.description = data["description"]
        self.type = "mp4" #TODO: link to downloader




class seasonclass(part):
    base_url="http://www.futurama-streaming.com/"

    def get(self, sf):
        episodes = get_episodes(self.base_url, self.seasonnumber)
        a = episodeclass(self, sf)
        a.episode_url = episodes[sf]["url"]
        return a

    def getAll(self):
        retour = []
        for loop in get_episodes(self.base_url, self.seasonnumber):
            retour.append(loop)
        return retour

class seasonsclass(part):
    def get(self, sf):
        a = seasonclass(self, sf)
        a.seasonnumber = sf
        return a

    def getAll(self):
        seasons = get_seasons()
        retour = []
        for loop in seasons:
            retour += str(loop)
        return retour

class mainclass(part):
    def get(self,sf):
        if sf == "season":
            return seasonsclass(self,sf)

    def getAll(self):
        return ["season"]
