from lxml import etree
from tool import parseHtml

from getClass import part, video

SEASON_PAGE_ALL = "https://www.tf1.fr/tf1/replay"
_cached_seasons = {}
def get_seasons():
    global _cached_seasons
    if _cached_seasons == {}:
        page = parseHtml(SEASON_PAGE_ALL)
        seasons = {}
        for div in page.iter("div"):
            if div.attrib.get("class") == "program key-list-programs":
                # get the name
                name = div.find("div").find("a").find("div").find("p").text
                url = div.find("a").attrib["href"]
                seasons[name] = {"name" : name,
                    "url" : url}
        _cached_seasons = seasons
    return _cached_seasons


def get_season(url):
    season = {
        "videos" : {}
    }

    # parse video
    page = parseHtml(url)

    for div in page.iter("div"):
        if div.attrib.get("class") == "video key-list-videos":
            # get the name
            desc = div.find("div").find("a").find("div")
            for title in desc:
                if title.attrib.get("class") == "title":
                    data = title.text
            try:
                dataSplited = data.split(" - ")
                typee = ""
                if len(dataSplited) == 4:
                    typee = dataSplited[0]
                    dataSplited = dataSplited[1:]
                serie = dataSplited[0]
                saison = int(dataSplited[1].split("Saison ")[1].split(" Episode")[0])
                episode = int(dataSplited[1].split("Episode ")[1])
                name = dataSplited[2]
                if typee != "":
                    name = typee + " - " + name
            except:
                name = data
                saison = 0
                serie = "unknwown"
                episode = 0
                typee = "unparsable"
            # get the url
            url = div.find("a").attrib.get("href")

            video = {
                "name" : name,
                "url" : url,
                "serie" : serie,
                "saison" : saison,
                "episode" : episode,
                "type" : typee
            }

            season["videos"][name] = video

    return season

class mainclass(part):
    """class with all replay related section :
list of section :
    replay : the replay, you need to then select a season"""
    def get(self,sf):
        if sf == "replay":
            return replayclass(self,sf)
        else:
            raise BaseException("unknow type of subfolder for the tf1 replay : "+sf)

    def getAll(self):
        return ["replay"]

class replayclass(part):
    """ every season avalaible for replay on tf1 replay """
    def getAll(self):
        render = []
        for loop in get_seasons():
            render.append(loop)
        return render

    def get(self,sf):
        seasons = get_seasons()
        serieName = sf.lower()
        serie = None
        for loop in seasons:
            if loop.lower() == serieName:
                serie = seasons[loop]

        if serie == None:
            raise BaseException("unable to find the tf1 searie " + serieName + " on tf1 replay.")
        render = seasonClass(self,sf)
        seasonClass.SerieData = serie

        return render

class seasonClass(part):
    """ a season, with all video available for replay on tf1 for a single season """
    def getAll(self):
        season = get_season(self.SerieData["url"])
        render = []
        for loop in season["videos"]:
            render.append(season["videos"][loop]["name"])
        return render

    def get(self,sf):
        v = None
        videos = get_season(self.SerieData["url"])["videos"]
        for loop in videos:
            if loop.lower() == sf.lower():
                v = videos[loop]
        if v == None:
            raise BaseException(sf+" dans la serie "+self.SerieData["name"]+" introuvable sur tf1 replay.")

        vid = video(self,sf)
        vid.name = v["name"]
        vid.getYTDL(v["url"])
        vid.serie = v["serie"]
        vid.episode = v["episode"]
        vid.type = v["type"]
        return vid
