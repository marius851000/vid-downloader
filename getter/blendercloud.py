from getClass import part, video
from tool import parseJson, parseHtml

class blenderVideo(video):
    def parse(self):
        page = parseHtml("https://cloud.blender.org/nodes/"+self.id+"/view")
        # for normal page
        for title in page.iter("h4"):
            if title.attrib.get("class") == "pt-4 mb-3":
                self.name = title.text

        for download in page.iter("ul"):
            if download.attrib.get("class") == "dropdown-menu dropdown-menu-right":
                dlurl = download.find("li").find("a").attrib.get("href")
                self.videoGetData = {"type":"url","url":dlurl}
                self.videoGetData["extension"] = dlurl.split("/")[4].split("?")[0].split(".")[-1]
        # for texture page
        # TODO:

class TreeClass(part):
    urlStart = "https://cloud.blender.org/nodes/"
    urlEnd = "/jstree?children=1"
    itering = "children"

    def getUrl(self):
        return self.urlStart+self.id+self.urlEnd

    def get(self,sf):
        dic = parseJson(self.getUrl())

        for loop in dic[self.itering]:
            if loop["text"] == sf:
                ide = loop["id"][2:]
                if loop["children"]:
                    a = TreeClass(self,sf)
                    ide = loop["id"][2:]
                    a.id = ide
                    return a
                else:
                    a = blenderVideo(self,sf)
                    a.id = ide
                    a.parse()
                    return a
        raise BaseException("unable to find "+sf+" on blender cloud")

    def getAll(self):
        dic = parseJson(self.getUrl())
        retour = []
        for loop in dic[self.itering]:
            retour.append(loop["text"])
        return retour

class ProjectClass(TreeClass):
    urlStart = "https://cloud.blender.org/p/"
    urlEnd = "/jstree"
    itering = "items"

class mainclass(part):
    project = ["characters","hdri","gallery"] + ["spring","hero","dailydweebs","agent-327","caminandes-3","glass-half","cosmos-laundromat","monkaa","caminandes-2","tears-of-steel","sintel","big-buck-bunny","elephants-dream"] # TODO: auto update
    # TODO: textures
    def get(self,sf):
        # TODO: check if sf exist
        if True:
            a = ProjectClass(self,sf)
            a.id = sf
            return a

    def getAll(self):
        return self.project
