from lxml import etree
from tool import parseHtml

from getClass import part, video

_base_url = "https://www.arte.tv/"

def isInHtmlClass(texte,html):
    if "class" in html.attrib:
        return texte in html.attrib["class"].split(" ")
    return False

def getCategoryArte(language):
    html = parseHtml(_base_url+language)

    for title in html.iter("li"):
        if isInHtmlClass("esspv3a6",title):
            div1 = title.find("a")
            if isInHtmlClass
            print(title.find("div").find("a").find("div").text)

class mainclass(part):
    def get(self,sf):
        if sf == "replay":
            return replayclass(self,sf)
        else:
            raise BaseException("unknow type of subfolder for the arte replay : "+sf)

    def getAll(self):
        return ["replay"]

class replayclass(part):
    # TODO: implement multiple language compatibility ( to final file or to url ? )
    """ every season avalaible for replay on arte replay """
    def getAll(self):
        return getCategoryArte("fr")
