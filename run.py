from argparse import ArgumentParser
from getClass import part, video
from uvideBaseClass import everyThing
import inspect
import json
import os
from tool import escape, escapemake


parser = ArgumentParser()
parser.add_argument("-u","--url",dest="url", help="the url of the serie/file to get ( a uvid url ).")
parser.add_argument("-f","--folder",dest="folder", help="the folder to put metadata in, finished with /")
parser.add_argument("-l","--list",dest="list", action="store_true", help="list the content in the folder rather than download it")
parser.add_argument("-s","--subfolder",dest="sub", action="store_true", help="respect the file hierchy chile writing file")
parser.add_argument("-o","--full",dest="oname",action="store_true",help="use the true name")
#TODO: add possibility to customize download path like in ytdl

args = parser.parse_args()
print(args.url)

#TODO: full rewrite of this shit
#TODO:
#TODO: partial download that do not corrupt the integral download -- maybe check for missing in folder and download in tempory folder

writeFolder = args.folder
a = everyThing()
got = a.getRecur(args.url)
if args.list:
    print(got.getAll())
else:
    if isinstance(got, video):
        videos = [got]
    else:
        videos = got.getAllDeep()
    ytdlf = open(args.folder+"ytdl.sh","w")
    for loop in videos:
        loop.parse()
        location = ""
        if not args.oname:
            location = str(loop.serie) + " - "+str(loop.saison)+" - "+str(loop.episode)+" - "+str(loop.name)
        else:
            location = str(loop.name)
        location = escape(location)
        add = ""
        if args.sub:
            temp = loop.getUrl().split("ŧ")[:-1]
            add = ""
            for element in temp:
                add += element + "/"
            os.makedirs(writeFolder+add,exist_ok=True)
        f=open(writeFolder+add+location+".json","w")
        json.dump(loop.getDict(),f)
        f.close()

        if loop.videoGetData["type"] == "ytdl":
            ytdlf.write("\nyoutube-dl "+loop.videoGetData["url"]+" --hls-prefer-native -o '"+add+location+"'")
        elif loop.videoGetData["type"] == "url":
            ytdlf.write("\nwget -O '"+add+location+"."+loop.videoGetData["extension"]+"' '"+loop.videoGetData["url"]+"'\n")
    ytdlf.close()
