#TODO: inheritance of parent information
class part:
    isVideo = False
    def __init__(self,child,loc):
        self.child = child
        self.loc = loc

    def getRecur(self,url,spliter="ŧ"):
        if url == "":
            return self
        splitted = url.split(spliter)
        toGet = splitted[0]
        if len(splitted) == 1:
            return self.get(toGet)
        else:
            rest = url[len(toGet)+1:]

            retour = self.get(toGet)
            if retour == None:
                raise BaseException("cant find "+toGet)
            return retour.getRecur(rest)


    def get(self,subfolder):
        return None

    def getChilds(self):
        a = self.child.getChilds()
        a.append(self)
        return a

    def getUrl(self):
        a = self.getChilds()
        url = ""
        for loop in a:
            url += loop.loc + "ŧ"
        return url[:-1]

    def getAllDeep(self,parentPercent=""):
        retour = [] # {"url":url,"data":data}
        nb = 0
        total = len(self.getAll())
        for loop in self.getAll():
            nb += 1
            temp = self.get(loop)
            percent = parentPercent + " " + str(nb)+"/"+str(total)
            print(percent)
            if issubclass(type(temp),video):
                retour.append(temp)
            else:
                retour.extend(temp.getAllDeep(percent))
        return retour

    def getAll(self):
        return []

class video(part):
    def __init__(self,child,loc):
        self.videoGetData = {"type":"empty"}
        self.name = None
        self.episode = None
        self.saison = None
        self.serie = None
        self.type = ""
        self.description = None
        self.child = child
        self.loc = loc

        self.parsed = False

    def getYTDL(self,url):
        self.videoGetData = {"type":"ytdl","url":url}

    def enhance(self,data):
        return data

    def parse(self):
        pass

    def getDict(self):
        if not self.parsed:
            self.parse()
        return self.enhance({
            "get" : self.videoGetData,
            "name" : self.name,
            "episode" : self.episode,
            "saison" : self.saison,
            "serie" : self.serie,
            "type" : self.type
        })
