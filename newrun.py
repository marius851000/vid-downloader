import os

class taskOrder:
    def __init__(self, order):
        self.order = order

    def perform(self):
        # the task should be able to continue without bordereffect whenever the previous crashed / stopped
        raise NotImplementedError

    def fork(self):
        forked = type(self)(self.order.copy())
        return forked

    def download(self, downloader, destination):
        import subprocess
        if downloader["type"] == "url":
            subprocess.check_call(["wget",downloader["url"],"-O",destination])
            return True
        else:
            raise

    def dump(self):
        import json
        return json.dumps(self.order)

class taskDownload(taskOrder):
    def perform(self):
        from uvideBaseClass import everyThing
        from getClass import part, video
        import json
        a = everyThing()
        content = a.getRecur(self.order["path"])
        if isinstance(content, video):
            fileData = content.getDict()
            print("downloading video",fileData)
            #TODO: a bit cryptic. Rewrite what is needed
            destinationFolder = os.path.join(self.order["folder"])
            filename = os.path.basename(destinationFolder)
            folder = os.path.dirname(destinationFolder)

            addtostart = ""
            if self.order["kodiname"] == True:
                addtostart += "S"+str(fileData["saison"])+"E"+str(fileData["episode"])+": "

            destinationFolder = os.path.join(folder, addtostart+filename)
            selectedDownloader = fileData["get"]
            destWithExtension = destinationFolder + "." + selectedDownloader["extension"]
            self.download(selectedDownloader, destWithExtension)
            f=open(destWithExtension+".json","w")
            json.dump(fileData,f)
            f.close()
            return {}
        elif isinstance(content, part):
            import subprocess
            childs = content.getAll()
            retour = []
            for child in childs:
                newpath = self.order["path"] + "ŧ" + child
                newfolder = os.path.join(self.order["folder"],child)
                forked = self.fork()
                forked.order["path"] = newpath
                forked.order["folder"] = newfolder
                retour.append(forked)
            return {"newtask":retour}
        else:
            raise



class taskPoll:
    def __init__(self):
        self.tasks = []

    def taskify(self, task):
        result = None
        if task["type"] == "download":
            result = taskDownload(task)
        if result == None:
            raise
        return result

    def add_task(self, task):
        #TODO: check integrity
        if isinstance(task, dict):
            taskified = self.taskify(task)
        else:
            taskified = task
        assert isinstance(taskified, taskOrder)
        self.add_task_row(taskified)

    def add_task_row(self, task):
        self.tasks.append(task)

    def remove_task(self, task):
        assert task in self.tasks
        self.tasks.remove(task)

    def get_random_task(self):
        import random
        task = random.randint(len(self.tasks))
        return self.tasks[task]

    @property
    def remaining_tasks(self):
        return len(self.tasks)

class taskPollDisk(taskPoll):
    def __init__(self, folder, finished):
        import subprocess
        self.folder = folder
        self.finished = finished
        subprocess.check_call(["mkdir","-p",self.folder])
        subprocess.check_call(["mkdir","-p",self.finished])
        self.tmp = "/tmp/"
        #TODO: read configuration
        self._tasks = {} # pair of hash : order
        self.done = []
        self.read_config()

    def get_hash(self, content):
        import hashlib
        import json
        import base64
        a = json.dumps(content).encode(encoding="ascii",errors="backslashreplace")
        return hashlib.sha256(a).hexdigest()

    def read_config(self):
        import json
        for loop in os.listdir(self.folder):
            f = open(os.path.join(self.folder,str(loop)))
            content = json.loads(f.read())
            f.close()
            self._tasks[loop] = self.taskify(content)
            assert self.get_hash(content) == loop
        self.done = os.listdir(self.finished)


    def newtemp(self):
        import random
        return os.path.join(self.tmp, str(random.randint(0, 10000000000000000000000000)))

    def loadfinishedrelative(self, folder):
        import json
        finishedFolder = os.path.join(folder,".finished")
        if not os.path.isdir(finishedFolder):
            return
        allfile = os.listdir(finishedFolder)
        for file in allfile:
            of = open(os.path.join(finishedFolder, file))
            parsed = json.loads(of.read())
            of.close()
            parsed["folder"] = os.path.abspath(os.path.join(folder,parsed["folder"]))
            hash = self.get_hash(parsed)
            self.done.append(hash)

    def add_task_row(self, task):
        import json
        import subprocess
        self.loadfinishedrelative(task.order["folder"])
        uid = self.get_hash(task.order)
        if uid in self.done:
            print(str(task.order)+" is already finished. skipping")
            return
        dumped = task.dump()
        path = os.path.join(self.folder, str(uid))
        temppath = self.newtemp()
        f = open(temppath, "w")
        f.write(dumped)
        f.close()
        subprocess.check_call(["mv",temppath,path])
        self._tasks[uid] = task

    def remove_task(self, task):
        import subprocess
        # first, we get the uid of the task
        uid = None
        for loop in self._tasks:
            if self._tasks[loop] == task:
                uid = loop
        assert uid != None
        assert "folder" in task.order
        otherfinished = os.path.join(task.order["folder"],".finished")
        subprocess.check_call(["mkdir","-p",otherfinished])
        othertask = task.fork()
        othertask.order["folder"] = "./"
        otherhash = self.get_hash(othertask.order)
        othertemp = self.newtemp()
        otherfile = open(othertemp, "w")
        otherfile.write(othertask.dump())
        otherfile.close()
        subprocess.check_call(["mv",othertemp,os.path.join(otherfinished,otherhash)])
        subprocess.check_call(["mv",os.path.join(self.folder,uid),os.path.join(self.finished,uid)])
        del self._tasks[uid]
        self.done.append(uid)

    def get_random_task(self):
        #TODO: better code, true random
        for loop in self._tasks:
            return self._tasks[loop]

    @property
    def remaining_tasks(self):
        return len(self._tasks)



def doBulk(task, taskToDo = taskPoll()):
    taskToDo.add_task(task)
    while taskToDo.remaining_tasks > 0:
        task = taskToDo.get_random_task()
        result = task.perform()
        if "newtask" in result:
            for loop in result["newtask"]:
                taskToDo.add_task(loop)
        taskToDo.remove_task(task)


if __name__ == '__main__':
    from pathlib import Path
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument("-u","--url",dest="url", help="the url of the serie/file to get ( a uvid-get url ).")
    parser.add_argument("-f","--folder",dest="folder", help="the destination folder. Will be created if not already existing")
    parser.add_argument("-l","--list",dest="list", action="store_true", help="list the content in the folder instead of downloading it")
    parser.add_argument("-k","--kodi",dest="kodiname", action="store_true", help="for TV show, include the episode and season in the form of S#E# to the beggining of the file.")
    args = parser.parse_args()
    assert type(args.url) == str

    if args.list:
        from uvideBaseClass import everyThing
        a = everyThing()
        b = a.getRecur(args.url).getAll()
        for loop in b:
            print(loop)
    else:
        home = str(Path.home())
        configFolder = os.path.abspath(os.path.join(home, ".uvid-get"))
        finishedFolder = os.path.abspath("./.finished")

        pollFolder = os.path.abspath("./.poll")

        assert type(args.url) == str
        assert type(args.folder) == str
        task = {
            "type": "download",
            "path": args.url,
            "folder": os.path.abspath(args.folder),
            "kodiname": args.kodiname
        }

        poll = taskPollDisk(pollFolder,finishedFolder)
        doBulk(task, taskToDo = poll)
