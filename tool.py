_pageCache={}
def getUrl(url, cookies):
    global _pageCache
    print("getting "+url)
    import requests
    if not url in _pageCache:
        sucess = False
        while not sucess:
            r = requests.get(url, cookies = cookies)
            if r.status_code == 200:
                html = r.text
                sucess = True
            else:
                import time
                print("failed")
                time.sleep(2)
        if sucess:
            _pageCache[url] = html
    else:
        sucess = True
        html = _pageCache[url]

    obtained = []
    #print(html)
    return html

def dumpUrl(url):
    #TODO: try to use a firefox base browser
    import subprocess
    return subprocess.check_output(["chromium","--headless","--disable-gpu",url,"--dump-dom"])

def parseHtml(url, dump = False, cookies = {}):
    if not dump:
        html = getUrl(url, cookies = cookies)
    else:
        if cookies != {}:
            print("warning: page dump do not accept cookies")
        html = dumpUrl(url)

    from lxml import etree
    data = etree.fromstring(html, etree.HTMLParser())

    return data

def parseJson(url):
    data = getUrl(url)

    import json

    return json.loads(data)

def escape(text):
    # replace / by &sla et & by &&&
    retour = ""
    for loop in text:
        if loop == "/":
            retour += "&sla"
        elif loop == "\\":
            retour += "&sb"
        elif loop == "&":
            retour += "&&&"
        elif loop == "'":
            retour += "&st"
        elif loop == '"':
            retour += "&rt"
        else:
            retour += loop
    return retour

def escapemake(text):
    prohibed = ["\\"," ",":","&"]
    retour = ""
    for loop in text:
        if loop in prohibed:
            retour += "\\"+loop
        else:
            retour += loop
    return retour
